package com.example.catsapi.apiservice.model
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ImageDTO (
        @SerialName("id") var id: String,
        @SerialName("url") var url: String ? = "",
        @SerialName("width") var width: Int,
        @SerialName("height") var height: Int
        )

