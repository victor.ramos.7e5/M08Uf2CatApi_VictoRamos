package com.example.catsapi.ui.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UIModel(
    val url: String?,
    val name: String,
    val desc: String,
    val countrycod: String ? = "",
    val temperament: String,
    val urlwiki: String ? = "",
): Parcelable


