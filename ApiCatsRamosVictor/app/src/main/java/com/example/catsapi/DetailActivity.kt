package com.example.catsapi

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import com.example.catsapi.ui.model.UIModel


class DetailActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val info = intent.getParcelableExtra<UIModel>("cat")

        setContent {
            if (info != null) {
                AffirmationAppDetail(info)
            }
        }

    }
}

@Composable
fun AffirmationAppDetail(cat:UIModel) {

        AffirmationDetail(cat)

}

@Composable
fun AffirmationDetail(cat:UIModel) {
    val context = LocalContext.current
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.fillMaxWidth()
        ) {
            Image(
                painter = rememberAsyncImagePainter(model = "https://cdn2.thecatapi.com/images/ed9.jpg"),
                contentDescription = "image",
                modifier = Modifier
                    .padding(16.dp)
                    .size(250.dp)
                    .clip(RoundedCornerShape(50)),
                contentScale = ContentScale.Crop,

                )
            Text(
                text = cat.name,color = Color.Blue ,
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.h3,
                modifier = Modifier.padding(12.dp)
            )
            Text(
                text = cat.temperament,
                style = MaterialTheme.typography.h6,
                modifier = Modifier.padding(16.dp),
                fontWeight = FontWeight.Bold

            )
            Text(
                text = cat.desc,fontSize = 18.sp,
                style = MaterialTheme.typography.body1,
                modifier = Modifier.padding(20.dp)
            )
            if( cat.countrycod != null){
                Text(
                    text = cat.countrycod,
                    style = MaterialTheme.typography.h6,
                    modifier = Modifier.padding(16.dp),
                    fontWeight = FontWeight.Bold
                )
            }


        }

    }
