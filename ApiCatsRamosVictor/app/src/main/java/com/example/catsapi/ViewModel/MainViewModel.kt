package com.example.catsapi.ViewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.catsapi.apiservice.CatsApi
import com.example.catsapi.ui.model.UIModel
import com.example.catsapi.ui.model.mappers.CatsDtoMapper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class MainViewModel: ViewModel() {
    private val _catsUIState = MutableStateFlow<List<UIModel>?>(null)
    val catsUIState: StateFlow<List<UIModel>?> = _catsUIState.asStateFlow()

    var mapper = CatsDtoMapper()


    init{
        getCats()
    }

    fun getCats() {
        viewModelScope.launch {
            val breedList = CatsApi.retrofitService.getBreed()
            //val imagesList = breedList.flatMap { CatsApi.retrofitService.getPhotos(it.id)}
            _catsUIState.value = mapper.map(breedList, emptyList())
        }
    }
}