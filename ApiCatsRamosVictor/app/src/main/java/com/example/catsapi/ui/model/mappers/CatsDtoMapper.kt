package com.example.catsapi.ui.model.mappers

import com.example.catsapi.data.apiservice.model.BreedDto
import com.example.catsapi.apiservice.model.ImageDTO
import com.example.catsapi.ui.model.UIModel

class CatsDtoMapper {
    fun map(listBreeds : List<BreedDto>, listImages : List<ImageDTO>): List<UIModel>{
        return listBreeds.mapIndexed { index, breed ->
            UIModel(
                url = "",
                name = breed.name,
                desc = breed.description,
                countrycod = breed.countryCode,
                temperament = breed.temperament,
                urlwiki = breed.wikipediaUrl
            )
        }
        return mutableListOf<UIModel>().apply {
            listBreeds.forEachIndexed{
                    index, breed ->
                add(
                    UIModel(
                    url = listImages[index].url,
                    name = breed.name,
                    desc = breed.description,
                    countrycod = breed.countryCode,
                    temperament = breed.temperament,
                        urlwiki = breed.wikipediaUrl
                )
                )
            }
        }
    }

}
