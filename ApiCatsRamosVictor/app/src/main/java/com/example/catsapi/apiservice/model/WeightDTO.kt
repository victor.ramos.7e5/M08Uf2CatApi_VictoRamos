package com.example.catsapi.apiservice.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class WeightDTO(
    @SerialName("imperial") var imperial: String,
    @SerialName("metric") var metric: String
)